const mongoose = require('mongoose');

const Schema = mongoose.Schema;

module.exports = new Schema({
  author: {
    firstName: String,
    lastName: String,
    fullName: String,
    gender: String,
  },
  description: String,
  publishDate: { type: Date, default: Date.now },
  genre: String,
  title: String,
  subTitle: String,
  thumbnail: String,
});
