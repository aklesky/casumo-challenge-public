const { makeExecutableSchema } = require('graphql-tools');

const book = require('./schema/book');
const { books } = require('./resolvers');

const executableSchema = makeExecutableSchema({
  typeDefs: [book],
  resolvers: books,
});
module.exports = executableSchema;
