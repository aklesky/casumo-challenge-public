module.exports = {
  bookQuery: {
    getBooks: async (parent, { limit, offset, genre, title, sortByAuthor }, { Books }) => {
      const query = {};
      if (genre) {
        query.genre = genre;
      }

      if (title) {
        query.title = new RegExp(title, 'ig');
      }

      const sort = {};
      if (sortByAuthor) {
        sort['author.fullName'] = 'asc';
      } else {
        sort.title = 'asc';
      }

      const books = await Books.find(query).collation({ locale: 'en', strength: 2 })
        .sort(sort)
        .limit(limit)
        .skip(offset);
      return books;
    },
    getGenres: async (parent, args, { Books }) => {
      const genres = await Books.distinct('genre');
      return genres.map(item => ({
        value: item,
      })).sort((a, b) => {
        if (a.value < b.value) return -1;
        if (a.value > b.value) return 1;
        return 0;
      });
    },
  },
};
