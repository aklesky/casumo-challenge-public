const Author = `
  type Author {
    firstName: String!
    lastName: String!
    fullName: String!
    gender: String!
  }
`;

module.exports = Author;
