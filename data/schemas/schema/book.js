const Author = require('./author');

const Book = `
  type Book {
    _id: ID!
    cursor: String
    description: String
    publishDate: String
    genre: String
    title: String
    thumbnail: String
    author: Author
  }

  type Genre {
    value: String
  }

  type bookQuery {
    getBooks(limit: Int!, offset: Int!, genre: String, title: String, sortByAuthor: Boolean): [Book]
    getGenres: [Genre]
  }


  schema {
    query: bookQuery
  }

`;

module.exports = () => [Book, Author];
