const Schemas = require('./schemas');
const Models = require('./models');

module.exports = {
  Schemas,
  Models,
};
