const mongoose = require('mongoose');
const paths = require('./env/paths');
const config = require('./env/config.json');

const Data = require(paths.dataPath);

mongoose.Promise = global.Promise;

const Books = mongoose.model('Books', Data.Models.Books);

module.exports = () => {
  mongoose.connect(config.mongoUri, (err) => {
    if (err) {
      throw err;
    }
  });
  const getContext = () => ({
    Books,
  });
  return {
    getContext,
  };
};
