
const express = require('express');
const bodyParser = require('body-parser');
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express');

const paths = require('./env/paths');

const { Schemas } = require(paths.dataPath);
const config = require('./env/config');

module.exports = (host, port, context, useGraphiq = false) => {
  const app = express();
  app.use('/', express.static(paths.appBuild));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Cache-Control', 'no-cache');
    next();
  });
  app.use(config.graphqlApi, bodyParser.json(), graphqlExpress({ schema: Schemas, context }));
  if (useGraphiq) {
    app.use(config.graphiql, graphiqlExpress({
      endpointURL: config.graphqlApi,
    }));
  }
  app.listen(port);
};
