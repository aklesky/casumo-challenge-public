const mongoConnection = require('./mongo');
const apolloServer = require('./apollo');
const devApp = require('./dev.server');
const config = require('./env/config');


const connection = mongoConnection();
const context = connection.getContext();

const env = process.env.NODE_ENV || 'production';

if (env === 'development') {
  apolloServer(config.host, config.devGraphQLPort, context, true);
  devApp();
  return;
}

const port = process.env.PORT || 3000;

apolloServer(config.host, port, context);

