# README #
Casumo JavaScript Challenge: Casumo loves lists!

### Quick summary ###
Casumo loves lists: Generate a list of one million books.

one million records have been generated with [faker](https://github.com/marak/Faker.js/) + 1000 records from google books api.
Frontend part is done by React and Apollo-React.
Backend part is fully working with apollo-express-server, graphql and mongo database.

### Sorting ###

* Sort by Book Name/Title (done) by default
* Sort By Author Name (done)
* Filter By Book Genre (done)
* Filter by Book Title (done)
* Sorting + Filtering are connected so they will work together

### UI Features ###

* Floating action button is switching sorting between Book Name and Author's Full Name
* Dropdown contains distinct Book Genres from db and will filter by Book Genre
* SearchBar will filter by Book Title
* Infinity Scroll List will display records by limit 15 and offset (*Limit is 15 per each*)


### Demo ###
for the demo purpose app is connected to [MLab](https://mlab.com) and mongoose config file is configured 
(**for local environment please check the documentation bellow**)

[APP DEMO](https://aklesky-casumo-challenge.herokuapp.com/)

Because of sandbox limitation there is only 164,468 records
NAME	DOCUMENTS	SIZE 
books	164,468		324.69 MB

**how to run the app locally you will find  documentation down by readme**


### CORE ###
* [react](https://facebook.github.io/react/)
* [redux](https://github.com/reactjs/redux)
* [react-redux](https://github.com/reactjs/react-redux)
* [redux-persist](https://github.com/rt2zz/redux-persist)
* [localforage](https://github.com/localForage/localForage)
* [graphql](http://graphql.org/)
* [react-apollo](https://github.com/apollographql/react-apollo)
* [mongo](https://www.mongodb.com/)
* [apollo-express-server](https://github.com/apollographql/apollo-server)
* [mongoose](http://mongoosejs.com/)


# Project Structure
* src
* src/app
* src/app/actions
* src/app/redux
* src/app/redux/reducers
* src/app/components
* src/app/components/books (**the main component to view and query books**)
* src/app/components/list (**infinity scroll list to view books**)
* src/app/config
* src/app/layouts
* src/app/providers
* src/app/containers
* src/app/translations
* src/app.js (**Entrypoint**)
* src/index.html (**Main Index html file**)
* data
* data/models (**Mongo database model**)
* data/schemas (**Graphql Schemas**)
* config (**Apollo Express Server, Mongoose initialization**)
* config/env
* resources
* resources/db.json.zip ( ~1.5GB after unzipping )


# Local installation / dev

for the debugging one million records local/dedicated mongo database is required
* unzip <project_dir>/resources/db.json.zip
* run shell command
```sh
mongoimport --host <host> --port <port> -d <database> -c books  --file=./db.json
```
* install npm packages
```sh
npm i
```
* edit <project_dir>/config/env/config.json
```code
{
  "mongoUri": "mongodb://<username>:<password>@<host>:<port>/<database>",
  ...
}
```
* run shell command
```sh
npm run dev
```
### NOT INCLUDED ###
* manifest.json
* favicon, apple icons, chrome icons
* service workers
* UI tests


### Node Version ###
* node v8.1.1
* npm 5.0.3

### Continuous Integration and Delivery ###
* Circle CI ( eslint test)
* Circle CI Continuous Delivery to Heroku

### UI ###
* [Material UI](http://http://www.material-ui.com/)
