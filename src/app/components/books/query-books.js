import { gql } from 'react-apollo';

const BooksQuery = gql`
  query books($limit: Int!, $offset: Int!, $genre: String, $title: String, $sortByAuthor: Boolean) {
      getBooks(limit: $limit, offset: $offset, genre: $genre, title: $title, sortByAuthor: $sortByAuthor) {
        _id
        cursor
        description
        publishDate
        genre
        title
        thumbnail
        author {
          firstName
          lastName
          fullName
        }
    }
  }
`;

const QueryOptions = {
  options() {
    return {
      variables: {
        title: null,
        offset: 0,
        limit: 15,
        sortByAuthor: false,
      },
      fetchPolicy: 'network-only',
    };
  },
  props({ data: { loading, getBooks, fetchMore, refetch, variables } }) {
    return {
      loading,
      getBooks,
      refetch,
      variables,
      loadMoreEntries() {
        return fetchMore({
          variables: {
            offset: getBooks.length,
          },
          updateQuery: (previousResult, { fetchMoreResult }) => {
            if (!fetchMoreResult) { return previousResult; }
            return Object.assign({}, previousResult, {
              loading: true,
              getBooks: [...previousResult.getBooks, ...fetchMoreResult.getBooks],
            });
          },
        });
      },
    };
  },
};

module.exports = {
  BooksQuery,
  QueryOptions,
};
