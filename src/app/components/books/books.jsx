import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import LibraryBooks from 'material-ui/svg-icons/av/library-books';
import Person from 'material-ui/svg-icons/social/person';
import { BooksQuery, QueryOptions } from './query-books';


import { List, ListItem } from '../list';
import Toolbar from '../toolbar/';
import styles from './styles';

class BooksComponent extends React.Component {
  static defaultProps = {
    getBooks: [],
  }
  static propTypes = {
    loading: PropTypes.bool.isRequired,
    loadMoreEntries: PropTypes.func.isRequired,
    getBooks: PropTypes.arrayOf(PropTypes.object),
    refetch: PropTypes.func.isRequired,
    variables: PropTypes.shape({
      sortByAuthor: PropTypes.bool.isRequired,
    }).isRequired,
  }
  constructor(props) {
    super(props);
    this.onFilterByGenre = this.onFilterByGenre.bind(this);
    this.onFilterByTitle = this.onFilterByTitle.bind(this);
    this.onSort = this.onSort.bind(this);
  }

  shouldComponentUpdate(newProps) {
    const { loading, getBooks } = newProps;
    if (loading !== this.props.loading) {
      return true;
    }
    if (getBooks.length !== this.props.getBooks.length) {
      return true;
    }
    return false;
  }

  onFilterByGenre(value) {
    this.props.refetch({
      genre: value || null,
    });
  }

  onFilterByTitle(value) {
    setTimeout(() => {
      this.props.refetch({
        title: value || null,
      });
    }, 300);
  }

  onSort() {
    this.props.refetch({
      sortByAuthor: !this.props.variables.sortByAuthor,
    });
  }

  render() {
    return (
      <div className='margin-vertical-5'>
        <FloatingActionButton
          onTouchTap={this.onSort}
          secondary
          style={styles.actionButton}
        >
          {this.props.variables.sortByAuthor ? (<LibraryBooks />) : (<Person />)}
        </FloatingActionButton>
        <Toolbar onFilterByGenre={this.onFilterByGenre} onFilterByTitle={this.onFilterByTitle} />
        {this.props.loading === false && this.props.getBooks.length > 0 && (
          <List threshold={768} loadMore={this.props.loadMoreEntries} hasNext>
            {this.props.getBooks.map(item => (<ListItem key={item._id} {...item} />))}
          </List>
        )}

      </div>
    );
  }
}


export default graphql(BooksQuery, QueryOptions)(BooksComponent);
