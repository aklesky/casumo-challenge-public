import React from 'react';
import PropTypes from 'prop-types';
import AppBar from 'material-ui/AppBar';
import { injectIntl, intlShape } from 'react-intl';

const Header = ({ intl, label }) => {
  const { formatMessage } = intl;
  return (
    <AppBar
      title={formatMessage({ id: label, defaultMessage: 'Aleksei Semiglasov App' })}
      showMenuIconButton={false}
    />
  );
};

Header.defaultProps = {
  label: 'appTitle',
};

Header.propTypes = {
  label: PropTypes.string,
  intl: intlShape.isRequired,
};

export default injectIntl(Header);
