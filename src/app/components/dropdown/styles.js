const defaultStyles = {
  style: {
    height: 48,
    marginRight: 10,
    maxWidth: 180,
  },
  inputStyle: {
    marginTop: 0,
  },
  floatingLabelShrinkStyle: {
    top: 18,
  },
  floatingLabelStyle: {
    top: 12,
    textTransform: 'capitalize',
  },
  errorStyle: {
    bottom: 4,
  },
};

export default {
  inputStyles: {
    ...defaultStyles,
    hintStyle: {
      textTransform: 'capitalize',
    },
  },
  dropdownStyles: {
    ...defaultStyles,
    hintStyle: {
      bottom: 2,
      fontSize: '60%',
      textTransform: 'capitalize',
    },
    labelStyle: {
      marginTop: 0,
      top: 12,
      height: 30,
      lineHeight: '26px',
    },
    iconStyle: {
      top: 0,
      bottom: 0,
      margin: 0,
      padding: 0,
    },
  },
};
