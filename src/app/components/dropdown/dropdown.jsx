import React from 'react';
import PropTypes from 'prop-types';
import SelectField from 'material-ui/SelectField';
import styles from './styles';

const Dropdown = ({
  label,
  children,
  onChange,
  value,
}) => (<SelectField
  floatingLabelText={label}
  onChange={(event, index, val) => onChange(val)}
  value={value}
  {...styles.dropdownStyles}
>
  {children}
</SelectField>);

Dropdown.defaultProps = {
  value: false,
};

Dropdown.propTypes = {
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  value: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default Dropdown;
