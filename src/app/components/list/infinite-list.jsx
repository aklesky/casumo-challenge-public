import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'material-ui/List';

class InfiniteList extends React.Component {
  static propTypes = {
    children: PropTypes
      .oneOfType([PropTypes.object, PropTypes.array])
      .isRequired,
    hasNext: PropTypes.bool,
    useCapture: PropTypes.bool,
    loadMore: PropTypes.func.isRequired,
    ref: PropTypes.func,
    threshold: PropTypes.number,
  };

  static defaultProps = {
    hasNext: true,
    ref: null,
    threshold: 250,
    useCapture: true,
  };

  constructor(props) {
    super(props);

    this.scrollListener = this.scrollListener.bind(this);
  }

  componentDidMount() {
    this.attachScrollListener();
  }

  componentDidUpdate() {
    this.attachScrollListener();
  }

  componentWillUnmount() {
    this.detachScrollListener();
  }

  detachScrollListener() {
    window.removeEventListener('scroll', this.scrollListener, this.props.useCapture);
    window.removeEventListener('resize', this.scrollListener, this.props.useCapture);
  }

  attachScrollListener() {
    if (!this.props.hasNext) {
      return;
    }

    window.addEventListener('scroll', this.scrollListener, this.props.useCapture);
    window.addEventListener('resize', this.scrollListener, this.props.useCapture);

    this.scrollListener();
  }

  scrollListener() {
    const el = this.scrollComponent;
    const scrollTop = (window.pageYOffset !== undefined) ?
      window.pageYOffset :
      (document.documentElement || document.body).scrollTop;

    const topPosition = this.calculateTopPosition(el);
    const offset = topPosition + (el.offsetHeight - scrollTop - window.innerHeight);
    if (offset < Number(this.props.threshold)) {
      this.detachScrollListener();
      this.props.loadMore();
    }
  }

  calculateTopPosition(el) {
    if (!el) {
      return 0;
    }
    return el.offsetTop + this.calculateTopPosition(el.offsetParent);
  }

  render() {
    const {
      children,
      ref,
    } = this.props;
    return (
      <section ref={(node) => {
        this.scrollComponent = node;
        if (ref) {
          ref(node);
        }
      }}
      >
        <List>
          {children}
        </List>
      </section>
    );
  }
}


export default InfiniteList;
