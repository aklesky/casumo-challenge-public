import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardHeader, CardTitle, CardText } from 'material-ui/Card';
import faker from 'faker';
import moment from 'moment';

export default class ListItem extends React.PureComponent {
  static defaultProps = {
    description: null,
    genre: null,
  }
  static propTypes = {
    title: PropTypes.string.isRequired,
    genre: PropTypes.string,
    thumbnail: PropTypes.string.isRequired,
    author: PropTypes.shape({
      fullName: PropTypes.string.isRequired,
    }).isRequired,
    publishDate: PropTypes.string.isRequired,
    description: PropTypes.string,
  }

  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
    };
  }

  handleExpandChange = (expanded) => {
    this.setState({ expanded });
  };

  handleToggle = (event, toggle) => {
    this.setState({ expanded: toggle });
  };

  handleExpand = () => {
    this.setState({ expanded: true });
  };

  handleReduce = () => {
    this.setState({ expanded: false });
  };

  render() {
    const description = this.props.description || faker.lorem.paragraphs(5);
    const publishDate = moment(this.props.publishDate).format('DD/MM/YYYY');
    return (
      <Card expanded={this.state.expanded} onExpandChange={this.handleExpandChange}>
        <CardText><strong>{this.props.genre}</strong></CardText>
        <CardHeader
          title={this.props.title}
          subtitle={this.props.author.fullName}
          avatar={this.props.thumbnail}
          actAsExpander
          showExpandableButton
        />
        <CardTitle title={this.props.title} subtitle={publishDate} expandable />
        <CardText expandable>{description}</CardText>
      </Card>
    );
  }
}
