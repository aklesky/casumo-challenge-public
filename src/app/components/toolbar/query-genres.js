import { gql } from 'react-apollo';

const GenresQuery = gql`
  query GenresQuery{
    getGenres {
    value
    }
  }
`;

export default GenresQuery;
