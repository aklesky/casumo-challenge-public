import React from 'react';
import PropTypes from 'prop-types';
// import IconMenu from 'material-ui/IconMenu';
// import IconButton from 'material-ui/IconButton';
// import FontIcon from 'material-ui/FontIcon';
import MenuItem from 'material-ui/MenuItem';
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import { graphql } from 'react-apollo';
import { injectIntl, intlShape, defineMessages } from 'react-intl';
import SearchBar from 'material-ui-search-bar';

import Dropdown from '../dropdown';
import GenresQuery from './query-genres';

const messages = defineMessages({
  genre: {
    id: 'genre',
    defaultMessage: 'Filter by Genre',
  },
  unselect: {
    id: 'unselect',
    defaultMessage: 'Cancel',
  },
});

class ToolbarFilter extends React.Component {
  static defaultProps = {
    hasFilters: false,
  }
  static propTypes = {
    intl: intlShape.isRequired,
    onFilterByGenre: PropTypes.func.isRequired,
    onFilterByTitle: PropTypes.func.isRequired,
    data: PropTypes.shape({
      loading: PropTypes.bool.isRequired,
      getGenres: PropTypes.arrayOf(PropTypes.object),
    }).isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {
      value: null,
      search: null,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  shouldComponentUpdate(newProps, newState) {
    if (newState.value !== this.state.value) {
      return true;
    }
    if (newState.search !== this.state.search) {
      return true;
    }

    if (newProps.data.getGenres.length !== this.props.data.getGenres) {
      return true;
    }

    return false;
  }

  handleChange = (value) => {
    this.setState({ value });
    this.props.onFilterByGenre(value);
  };

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <Toolbar>
        <ToolbarGroup>
          <Dropdown
            label={formatMessage(messages.genre)}
            onChange={this.handleChange}
            value={this.state.value || null}
            maxWidth={200}
          >
            {this.state.value && (<MenuItem value={false} primaryText={formatMessage(messages.unselect)} />)}
            {!this.props.data.loading && (this.props.data.getGenres.map(item => (
              <MenuItem key={item.value} value={item.value} primaryText={item.value} />
            )))}
          </Dropdown>
        </ToolbarGroup>
        <ToolbarGroup>
          <SearchBar
            onChange={(value) => {
              this.setState({ search: value });
              this.props.onFilterByTitle(value);
            }}
            onRequestSearch={() => this.props.onFilterByTitle(this.state.search)}
            style={{
              margin: '0 auto',
              maxWidth: 200,
            }}
          />
        </ToolbarGroup>
      </Toolbar>
    );
  }
}

export default injectIntl(graphql(GenresQuery)(ToolbarFilter));
