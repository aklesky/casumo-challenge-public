import { App } from '../../redux/reducers';

export const appIsRestored = state => ({
  type: App.UPDATE,
  payload: {
    restored: state,
  },
});

export const showNotificationMessage = data => (dispatch) => {
  dispatch({
    type: App.UPDATE,
    payload: data,
  });
};
