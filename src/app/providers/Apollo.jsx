import React from 'react';
import PropTypes from 'prop-types';
import { ApolloProvider } from 'react-apollo';

const Apollo = ({ client, store, children }) => (
  <ApolloProvider client={client} store={store}>
    {children}
  </ApolloProvider>
);

Apollo.defaultProps = {
  children: null,
};

Apollo.propTypes = {
  client: PropTypes.shape({}).isRequired,
  store: PropTypes.shape({}).isRequired,
  children: PropTypes.element,
};

export default Apollo;
