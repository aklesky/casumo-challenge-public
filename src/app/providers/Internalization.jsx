import React from 'react';
import PropTypes from 'prop-types';
import { IntlProvider, addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import * as Translations from '../translations';

addLocaleData([...en]);

const Internalization = ({ locale, children }) => (
  <IntlProvider
    locale={locale}
    messages={Translations[locale]}
  >
    {children}
  </IntlProvider>
);

Internalization.defaultProps = {
  locale: 'en',
};

Internalization.propTypes = {
  children: PropTypes.element.isRequired,
  locale: PropTypes.string,
};

export default Internalization;
