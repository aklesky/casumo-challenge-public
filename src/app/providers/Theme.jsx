import React from 'react';
import PropTypes from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { muiTheme } from '../theme';

const Theme = ({ children }) => (
  <MuiThemeProvider muiTheme={muiTheme}>
    {children}
  </MuiThemeProvider>
);

Theme.defaultProps = {
  children: null,
};

Theme.propTypes = {
  children: PropTypes.element,
};

export default Theme;
