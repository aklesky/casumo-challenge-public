import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { lightBlue900, teal600, deepOrange500, blueGrey800 } from 'material-ui/styles/colors';
import { mui } from './metrics';


const muiTheme = getMuiTheme({
  ...mui,
  palette: {
    primary1Color: lightBlue900,
    accent1Color: teal600,
  },
  textField: {
    errorColor: deepOrange500,
  },
  raisedButton: {
    primaryColor: blueGrey800,
  },
});

module.exports = muiTheme;
