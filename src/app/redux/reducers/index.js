import app, {
  actionTypes as App,
} from './app';

export default {
  app,
};

export {
  App,
};
