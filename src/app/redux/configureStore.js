import { applyMiddleware, compose, createStore, combineReducers } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import { createLogger } from 'redux-logger';
import { ApolloClient } from 'react-apollo';
import localforage from 'localforage';
import { storeConfig } from '../config';
import appReducers from './reducers';

const client = new ApolloClient();
const reducers = combineReducers({
  ...appReducers,
  apollo: client.reducer(),
});

export default (initialState = {}, onComplete) => {
  const middleware = [];

  if (process.env.NODE_ENV === 'development') {
    middleware.push(
      createLogger({
        level: 'info',
        collapsed: true,
      }),
    );
  }

  const enhancers = [autoRehydrate()];
  const composeEnhancers = compose;

  const store = createStore(
    reducers,
    initialState,
    composeEnhancers(applyMiddleware(...middleware), ...enhancers),
  );

  persistStore(
    store,
    {
      storage: localforage,
      transforms: [],
      keyPrefix: storeConfig.prefix,
    },
  );
  onComplete(store)(client);
};
