import storeConfig from './store';
import apolloConfig from './apollo';

module.exports = {
  storeConfig,
  apolloConfig,
};
