import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { MainLayout } from '../layouts';
import { Header } from '../components';
import Books from '../components/books/books';

import { IntlProvider } from '../providers';

class AppContainer extends React.PureComponent {
  static propTypes = {
    locale: PropTypes.string.isRequired,
  };

  render() {
    const { locale } = this.props;
    return (
      <IntlProvider
        locale={locale}
      >
        <MainLayout>
          <Header />
          <Books />
        </MainLayout>
      </IntlProvider >
    );
  }
}


const mapStateToProps = state => ({
  ...state.app,
});

export default connect(mapStateToProps)(AppContainer);
