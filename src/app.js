
import React from 'react';
import { render } from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import createStore from './app/redux/configureStore';
import AppContainer from './app/containers/AppContainer';
import { ApolloProvider, ThemeProvider } from './app/providers/';

import './static/css/app.scss';

injectTapEventPlugin();
const INITIAL_STATE = {};
createStore(INITIAL_STATE, store => (apolloClient) => {
  render(
    <ApolloProvider client={apolloClient} store={store}>
      <ThemeProvider>
        <AppContainer />
      </ThemeProvider>
    </ApolloProvider>,
    document.getElementById('app'),
  );
});
